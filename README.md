# Competitive-Programming-Guide-books
Competitive programming books and papers for pratice
<ul>
  <li>Competitive Programmer’s Handbook (Antti Laaksonen)</li>
   <li>Competitive Programming (Steven & Felix,)</li>
  <li>The Hitchhiker’s Guide to the Programming Contests</li>
  
  <li>DATA STRUCTURES USING C++ (D.S. MALIK)</li>
  <li>Data Structures & Algorithms in Java (Robert Lafore)</li>
  <li>Data Structures And Algorithms Made Easy In JAVA (Narasimha Karumanchi)</li>
  
  <li>Reasoning Book for placements (PlacementSeason)</li>  
  <li>Synonyms & Antonyms (PlacementSeason)</li>
   <li> Quant Formula Book (PlacementSeason)</li>  
  <li>Aplitude IndiaBix</li> 
  
  <li>ELEMENTS OF PROGRAMMING INTERVIEWS Java (ADNAN AZIZ, TSUNG-HSIEN LEE, AMIT PRAKASH)</li>
   <li>Data Science Interview Questions</li>
    
  
 </ul>
 
 
 <h5>Pratice questions from <a href="https://practice.geeksforgeeks.org/courses/SudoPlacement/">sudo-Placement</a> course from GeeksForGeeks</h5>
 <h5>Follow up the notes from <a href="https://www.tutorialspoint.com/data_structures_algorithms/index.htm">Tutorialspoint DSA</a></h5>
 <h5><a href="https://visualgo.net/en"> Visualize Algorithms</a></h5>
 <h5>Data Structures-Mycodeschool <a href="https://www.youtube.com/watch?v=92S4zgXN17o&list=PL2_aWCzGMAwI3W_JlcBbtYTwiQSsOTa6P">Youtube Playlist</a></h5>
